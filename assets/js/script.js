/* Add click event*/
document.querySelector('.convert-word').addEventListener("click", (e) => {
	e.preventDefault();
	convertToPigLatin();
})

/* Function to conver the string to pig latin*/
function convertToPigLatin() {
	let text = document.querySelector('.text-field').value;
	let convertSentence  = "";
	let textArray = text.split(' ');
	/* Regx for find word first letter is vowel or not*/
	let regx = /\b[aueio]\w*/g;
	textArray ? textArray.map(word => {
		if(word.match(regx)) {
			word = `${word}way`;
			convertSentence = `${convertSentence} ${word}`;
		} else {
			let firstChar = word[0];
			word = word.substr(1);
			word = `${word}${firstChar}ay`;
			convertSentence = `${convertSentence} ${word}`;
		}
	}) : null
	document.querySelector('.converted-string').innerText = "";
	document.querySelector('.converted-string').innerText = convertSentence;
}